from django.urls import path
from . import views
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [

    # api/hospitalcompare/facility
    path('facility/', views.FacilityViewAll.as_view()),

    # api/hospitalcompare/facility/<asc_name>
    path('facility/<asc_name>', views.FacilityName.as_view()),

    # api/hospitalcompare/state
    path('state/', views.StateViewAll.as_view()),

    # api/hospitalcompare/state/<state>
    path('state/<state>', views.StateName.as_view()),

    # api/hospitalcompare/cdhospital
    path('cdhospital/', views.ComplicationDeathHospitalViewAll.as_view()),

    # api/hospitalcompare/cdhospital/<hospital_name>
    path('cdhospital/<hospital_name>', views.ComplicationDeathHospitalName.as_view()),

    # api/hospitalcompare/cdnational
    path('cdnational/', views.ComplicationDeathNationalViewAll.as_view()),

    # api/hospitalcompare/cdnational/<measure_name>
    path('cdnational/<measure_name>', views.ComplicationDeathNationalName.as_view()),

    # api/hospitalcompare/cdstate
    path('cdstate/', views.ComplicationDeathStateViewAll.as_view()),

    # api/hospitalcompare/cdstate/<state>
    path('cdstate/<state>', views.ComplicationDeathStateName.as_view()),

    # api/hospitalcompare/haihospital
    path('haihospital/', views.HealthcareAssociatedInfectionsHospitalViewAll.as_view()),

    # api/hospitalcompare/haihospital/<name>
    path('haihospital/<hospital_name>', views.HealthcareAssociatedInfectionsHospitalName.as_view()),


]

urlpatterns = format_suffix_patterns(urlpatterns)