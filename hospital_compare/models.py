from django.db import models

class Facility(models.Model):
    asc_name = models.CharField(max_length=79, blank=True, null=True)
    provider_id = models.CharField(max_length=10, blank=True, null=True)
    npi = models.IntegerField(blank=True, null=True)
    city = models.CharField(max_length=22, blank=True, null=True)
    state = models.CharField(max_length=2, blank=True, null=True)
    zip_code = models.CharField(max_length=2, blank=True, null=True)
    year = models.IntegerField(blank=True, null=True)
    asc_1_measure_rate = models.CharField(max_length=7, blank=True, null=True)
    asc_1_footnote = models.CharField(max_length=2, blank=True, null=True)
    asc_2_measure_rate = models.CharField(max_length=6, blank=True, null=True)
    asc_2_footnote = models.CharField(max_length=2, blank=True, null=True)
    asc_3_measure_rate = models.CharField(max_length=6, blank=True, null=True)
    asc_3_footnote = models.CharField(max_length=2, blank=True, null=True)
    asc_4_measure_rate = models.CharField(max_length=7, blank=True, null=True)
    asc_4_footnote = models.CharField(max_length=2, blank=True, null=True)
    asc_5_measure_rate = models.CharField(max_length=8, blank=True, null=True)
    asc_5_footnote = models.CharField(max_length=2, blank=True, null=True)
    asc_1_5_encounter_start_date = models.CharField(max_length=8, blank=True, null=True)
    asc_1_5_encounter_end_date = models.CharField(max_length=10, blank=True, null=True)
    asc_6_ss_checklist = models.CharField(max_length=3, blank=True, null=True)
    asc_6_footnote = models.CharField(max_length=1, blank=True, null=True)
    asc_7_volume = models.CharField(max_length=6, blank=True, null=True)
    asc_7_respiratory = models.CharField(max_length=5, blank=True, null=True)
    asc_7_eye = models.CharField(max_length=6, blank=True, null=True)
    asc_7_genitourinary = models.CharField(max_length=6, blank=True, null=True)
    asc_7_musculoskeletal = models.CharField(max_length=6, blank=True, null=True)
    asc_7_gastrointestinal = models.CharField(max_length=6, blank=True, null=True)
    asc_7_nervous_system = models.CharField(max_length=6, blank=True, null=True)
    asc_7_skin = models.CharField(max_length=5, blank=True, null=True)
    asc_7_footnote = models.CharField(max_length=1, blank=True, null=True)
    asc_6_7_encounter_start_date = models.CharField(max_length=8, blank=True, null=True)
    asc_6_7_encounter_end_date = models.CharField(max_length=10, blank=True, null=True)
    asc_8_rate = models.CharField(max_length=5, blank=True, null=True)
    asc_8_footnote = models.CharField(max_length=1, blank=True, null=True)
    asc_8_encounter_date = models.CharField(max_length=9, blank=True, null=True)
    asc_9_rate = models.CharField(max_length=5, blank=True, null=True)
    asc_9_footnote = models.CharField(max_length=1, blank=True, null=True)
    asc_10_rate = models.CharField(max_length=5, blank=True, null=True)
    asc_10_footnote = models.CharField(max_length=1, blank=True, null=True)
    asc_9_10_encounter_start_date = models.CharField(max_length=8, blank=True, null=True)
    asc_9_10_encounter_end_date = models.CharField(max_length=10, blank=True, null=True)
    asc_11_rate = models.CharField(max_length=5, blank=True, null=True)
    asc_11_footnote = models.CharField(max_length=1, blank=True, null=True)
    asc_11_encounter_start_date = models.CharField(max_length=8, blank=True, null=True)
    asc_11_encounter_end_date = models.CharField(max_length=10, blank=True, null=True)
    asc_12_total_cases = models.CharField(max_length=4, blank=True, null=True)
    asc_12_performance_category = models.CharField(max_length=35, blank=True, null=True)
    asc_12_interval_lower_limit = models.CharField(max_length=4, blank=True, null=True)
    asc_12_interval_upper_limit = models.CharField(max_length=4, blank=True, null=True)
    asc_12_footnote = models.CharField(max_length=1, blank=True, null=True)
    asc_12_encounter_start_date = models.CharField(max_length=8, blank=True, null=True)
    asc_12_encounter_end_date = models.CharField(max_length=10, blank=True, null=True)
    asc_12_rshv_rate = models.CharField(max_length=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'asqm_facility'
        verbose_name_plural = "Ambulatory Surgical Quality Measures - Facility"

    def __str__(self):
        return self.asc_name    

class State(models.Model):
    asc_2_measure_state_rate = models.FloatField(blank=True, null=True)
    avg_eye_state = models.CharField(max_length=7, blank=True, null=True)
    asc_6_state_pct = models.CharField(max_length=7, blank=True, null=True)
    median_asc_9_state_rate = models.CharField(max_length=7, blank=True, null=True)
    median_respiratory_state = models.CharField(max_length=7, blank=True, null=True)
    avg_respiratory_state = models.CharField(max_length=7, blank=True, null=True)
    avg_asc_10_state_rate = models.CharField(max_length=7, blank=True, null=True)
    asc_1_measure_state_rate = models.FloatField(blank=True, null=True)
    avg_musculoskeletal_state = models.CharField(max_length=7, blank=True, null=True)
    asc_12_worse = models.IntegerField(blank=True, null=True)
    median_nervous_system_state = models.CharField(max_length=7, blank=True, null=True)
    median_skin_state = models.CharField(max_length=7, blank=True, null=True)
    avg_gastrointestinal_state = models.CharField(max_length=7, blank=True, null=True)
    avg_asc_7_state = models.CharField(max_length=7, blank=True, null=True)
    avg_genitourinary_state = models.CharField(max_length=7, blank=True, null=True)
    avg_skin_state = models.CharField(max_length=7, blank=True, null=True)
    asc5_measure_state_rate = models.CharField(max_length=7, blank=True, null=True)
    asc4_measure_state_rate = models.FloatField(blank=True, null=True)
    asc_12_better = models.IntegerField(blank=True, null=True)
    avg_asc_9_state_rate = models.CharField(max_length=7, blank=True, null=True)
    median_asc_10_state_rate = models.CharField(max_length=7, blank=True, null=True)
    median_asc_7_state = models.CharField(max_length=7, blank=True, null=True)
    asc_12_no_different = models.IntegerField(blank=True, null=True)
    avg_nervous_system_state = models.CharField(max_length=7, blank=True, null=True)
    median_musculoskeletal_state = models.CharField(max_length=7, blank=True, null=True)
    median_eye_state = models.CharField(max_length=7, blank=True, null=True)
    median_asc_11_state_rate = models.CharField(max_length=7, blank=True, null=True)
    median_asc_8_state_rate = models.FloatField(blank=True, null=True)
    asc3_measure_state_rate = models.FloatField(blank=True, null=True)
    median_genitourinary_state = models.CharField(max_length=7, blank=True, null=True)
    avg_asc_11_state_rate = models.CharField(max_length=7, blank=True, null=True)
    avg_asc_8_state_rate = models.FloatField(blank=True, null=True)
    asc_12_too_small = models.IntegerField(blank=True, null=True)
    median_gastrointestinal_state = models.CharField(max_length=7, blank=True, null=True)
    state = models.CharField(max_length=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'asqm_state'
        verbose_name_plural = "Ambulatory Surgical Quality Measures - State"

    def __str__(self):
        return self.state

class Complication_Death_Hospital(models.Model):
    computed_region_csmy_5jwy = models.IntegerField(blank=True, null=True)
    computed_region_f3tr_pr43 = models.IntegerField(blank=True, null=True)
    computed_region_nwen_78xc = models.IntegerField(blank=True, null=True)
    address = models.CharField(max_length=33, blank=True, null=True)
    city = models.CharField(max_length=16, blank=True, null=True)
    compared_to_national = models.CharField(max_length=36, blank=True, null=True)
    county_name = models.CharField(max_length=15, blank=True, null=True)
    denominator = models.CharField(max_length=13, blank=True, null=True)
    footnote = models.CharField(max_length=145, blank=True, null=True)
    higher_estimate = models.CharField(max_length=13, blank=True, null=True)
    hospital_name = models.CharField(max_length=50, blank=True, null=True)
    location_coordinates = models.FloatField(blank=True, null=True)
    location_type = models.CharField(max_length=5, blank=True, null=True)
    location_address = models.CharField(max_length=33, blank=True, null=True)
    location_city = models.CharField(max_length=16, blank=True, null=True)
    location_state = models.CharField(max_length=2, blank=True, null=True)
    lower_estimate = models.CharField(max_length=13, blank=True, null=True)
    measure_end_date = models.CharField(max_length=23, blank=True, null=True)
    measure_id = models.CharField(max_length=25, blank=True, null=True)
    measure_name = models.CharField(max_length=72, blank=True, null=True)
    measure_start_date = models.CharField(max_length=23, blank=True, null=True)
    phone_number = models.IntegerField(blank=True, null=True)
    provider_id = models.IntegerField(blank=True, null=True)
    score = models.CharField(max_length=13, blank=True, null=True)
    state = models.CharField(max_length=2, blank=True, null=True)
    zip_code = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cd_hospital'
        verbose_name_plural = "Complications and Deaths - Hospital"

    def __str__(self):
        return self.hospital_name

class Complication_Death_National(models.Model):
    footnote = models.CharField(max_length=66, blank=True, null=True)
    measure_end_date = models.CharField(max_length=23, blank=True, null=True)
    measure_id = models.CharField(max_length=25, blank=True, null=True)
    measure_name = models.CharField(max_length=72, blank=True, null=True)
    measure_start_date = models.CharField(max_length=23, blank=True, null=True)
    national_rate = models.FloatField(blank=True, null=True)
    number_of_hospitals_better = models.IntegerField(blank=True, null=True)
    number_of_hospitals_same = models.IntegerField(blank=True, null=True)
    number_of_hospitals_too_few = models.CharField(max_length=13, blank=True, null=True)
    number_of_hospitals_worse = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cd_national'
        verbose_name_plural = "Complications and Deaths - National"

    def __str__(self):
        return self.measure_name

class Complication_Death_State(models.Model):
    footnote = models.CharField(max_length=124, blank=True, null=True)
    measure_end_date = models.CharField(max_length=23, blank=True, null=True)
    measure_id = models.CharField(max_length=25, blank=True, null=True)
    measure_name = models.CharField(max_length=72, blank=True, null=True)
    measure_start_date = models.CharField(max_length=23, blank=True, null=True)
    number_of_hospitals_better = models.CharField(max_length=13, blank=True, null=True)
    number_of_hospitals_same = models.CharField(max_length=13, blank=True, null=True)
    number_of_hospitals_too_few = models.CharField(max_length=13, blank=True, null=True)
    number_of_hospitals_worse = models.CharField(max_length=13, blank=True, null=True)
    state = models.CharField(max_length=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cd_state'
        verbose_name_plural = "Complications and Deaths - State"

    def __str__(self):
        return self.state

class Healthcare_Associated_Infections_Hospital(models.Model):
    computed_region_csmy_5jwy = models.IntegerField(blank=True, null=True)
    computed_region_f3tr_pr43 = models.IntegerField(blank=True, null=True)
    computed_region_nwen_78xc = models.IntegerField(blank=True, null=True)
    address = models.CharField(max_length=35, blank=True, null=True)
    city = models.CharField(max_length=16, blank=True, null=True)
    compared_to_national = models.CharField(max_length=36, blank=True, null=True)
    county_name = models.CharField(max_length=20, blank=True, null=True)
    footnote = models.CharField(max_length=145, blank=True, null=True)
    hospital_name = models.CharField(max_length=48, blank=True, null=True)
    location_coordinates = models.FloatField(blank=True, null=True)
    location_type = models.CharField(max_length=5, blank=True, null=True)
    location_address = models.CharField(max_length=35, blank=True, null=True)
    location_city = models.CharField(max_length=16, blank=True, null=True)
    location_state = models.CharField(max_length=2, blank=True, null=True)
    measure_end_date = models.CharField(max_length=23, blank=True, null=True)
    measure_id = models.CharField(max_length=15, blank=True, null=True)
    measure_name = models.CharField(max_length=80, blank=True, null=True)
    measure_start_date = models.CharField(max_length=23, blank=True, null=True)
    phone_number = models.IntegerField(blank=True, null=True)
    provider_id = models.IntegerField(blank=True, null=True)
    score = models.CharField(max_length=13, blank=True, null=True)
    state = models.CharField(max_length=2, blank=True, null=True)
    zip_code = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'hai_hospital'       
        verbose_name_plural = "Healthcare Associated Infections - Hospital" 

    def __str__(self):
        return self.hospital_name                               
