from django.contrib import admin
from .models import *

admin.site.register(Facility)
admin.site.register(State)
admin.site.register(Complication_Death_Hospital)
admin.site.register(Complication_Death_National)
admin.site.register(Complication_Death_State)
admin.site.register(Healthcare_Associated_Infections_Hospital)
