from django.shortcuts import render
from rest_framework import generics
from .models import *
from .serializers import *

## Ambulatory Surgical Quality Measures - Facility
#Get ALL
class FacilityViewAll(generics.ListAPIView):
    queryset = Facility.objects.all()
    serializer_class = FacilitySerializer

#GET name
class FacilityName(generics.ListAPIView):
    serializer_class = FacilitySerializer

    def get_queryset(self):
        name = self.kwargs['asc_name']
        return Facility.objects.filter(asc_name=name)  


## Ambulatory Surgical Quality Measures - State
#Get ALL
class StateViewAll(generics.ListAPIView):
    queryset = State.objects.all()
    serializer_class = StateSerializer

#GET name
class StateName(generics.ListAPIView):
    serializer_class = StateSerializer

    def get_queryset(self):
        name = self.kwargs['state']
        return State.objects.filter(state=name) 

## Complications and Deaths - Hospital
#Get ALL
class ComplicationDeathHospitalViewAll(generics.ListAPIView):
    queryset = Complication_Death_Hospital.objects.all()
    serializer_class = ComplicationDeathHospitalSerializer

#GET name
class ComplicationDeathHospitalName(generics.ListAPIView):
    serializer_class = ComplicationDeathHospitalSerializer

    def get_queryset(self):
        name = self.kwargs['hospital_name']
        return Complication_Death_Hospital.objects.filter(hospital_name=name) 

## Complications and Deaths - National
#Get ALL
class ComplicationDeathNationalViewAll(generics.ListAPIView):
    queryset = Complication_Death_National.objects.all()
    serializer_class = ComplicationDeathNationalSerializer

#GET name
class ComplicationDeathNationalName(generics.ListAPIView):
    serializer_class = ComplicationDeathNationalSerializer

    def get_queryset(self):
        name = self.kwargs['measure_name']
        return Complication_Death_National.objects.filter(measure_name=name) 

## Complications and Deaths - State
#Get ALL
class ComplicationDeathStateViewAll(generics.ListAPIView):
    queryset = Complication_Death_State.objects.all()
    serializer_class = ComplicationDeathStateSerializer

#GET name
class ComplicationDeathStateName(generics.ListAPIView):
    serializer_class = ComplicationDeathStateSerializer

    def get_queryset(self):
        name = self.kwargs['state']
        return Complication_Death_State.objects.filter(state=name) 

## Healthcare Associated Infections - Hospital
#Get ALL
class HealthcareAssociatedInfectionsHospitalViewAll(generics.ListAPIView):
    queryset = Healthcare_Associated_Infections_Hospital.objects.all()
    serializer_class = HealthcareAssociatedInfectionsHospitalSerializer

#GET name
class HealthcareAssociatedInfectionsHospitalName(generics.ListAPIView):
    serializer_class = HealthcareAssociatedInfectionsHospitalSerializer

    def get_queryset(self):
        name = self.kwargs['hospital_name']
        return Healthcare_Associated_Infections_Hospital.objects.filter(hospital_name=name)                                 
