from django.apps import AppConfig


class FacilityApiAppConfig(AppConfig):
    name = 'facility_api_app'
