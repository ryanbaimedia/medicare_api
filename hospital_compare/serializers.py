from rest_framework import serializers
from .models import *

class FacilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Facility
        fields = ('asc_name', 
                  'provider_id', 
                  'city', 
                  'state', 
                  'zip_code', 
                  'asc_12_total_cases', 
                  'asc_12_performance_category',)

class StateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Facility
        fields = ('__all__')

class ComplicationDeathHospitalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Facility
        fields = ('__all__')

class ComplicationDeathNationalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Facility
        fields = ('__all__')

class ComplicationDeathStateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Facility
        fields = ('__all__')

class HealthcareAssociatedInfectionsHospitalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Facility
        fields = ('__all__')                   