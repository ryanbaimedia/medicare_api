## INSTALLATION

* Download and install Anaconda
* Create an environment in Anaconda
* Open the created environment in terminal
* Navigate to project folder  

```sh
#Install Dependencies
pip install -r requirements.txt

#Migrate Files
python manage.py migrate

#Start the server
python manage.py runserver


#http://localhost:8000 --> Admin
```
### API Endpoints (at the moment)

* http://localhost:8000/api/hospitalcompare/facility
* http://localhost:8000/api/hospitalcompare/facility/asc_name

* http://localhost:8000/api/hospitalcompare/state
* http://localhost:8000/api/hospitalcompare/state/state

* http://localhost:8000/api/hospitalcompare/cdhospital
* http://localhost:8000/api/hospitalcompare/cdhospital/hospital_name

* http://localhost:8000/api/hospitalcompare/cdnational
* http://localhost:8000/api/hospitalcompare/cdnational/measure_name

* http://localhost:8000/api/hospitalcompare/cdstate
* http://localhost:8000/api/hospitalcompare/cdstate/state

* http://localhost:8000/api/hospitalcompare/haihospital
* http://localhost:8000/api/hospitalcompare/haihospital/hospital_name



### Deploy To Heroku
[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

* add your dev/production configurations in config.py
